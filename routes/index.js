const express = require('express');
const router = express.Router();
const request = require('request');
const promise = require('promise');
const cheerio = require('cheerio');
const furl = require('friendly-url');
// const JSDOM = require('jsdom');
// let $ = require('jquery');
/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Express'
    });
});

router.get('/test', function(req, res, next) {
    request('http://google.com', function(err, response, body) {
        if (err) {
            // res.send(err);
        } else {
            res.send(body);
        }
    });
});

router.get('/design', function(req, res, next) {
    let data = {"info_list":{"model":"Model","color":"Color","storage":"Storage"},"general_list":{"sku":"SKU","camera-back":"Camera Back","camera-front":"Camera Front","condition":"Condition","screen-size-inches":"Screen Size (inches)","model":"Model","network-connections":"Network Connections","operating-system":"Operating System","operating-system-version":"Operating System Version","phone-features":"Phone Features","ppi":"PPI","processor-type":"Processor Type","ram-memory":"RAM memory","resolution":"Resolution","screen-type":"Screen Type","storage-capacity":"Storage Capacity","type-of-battery":"Type of Battery","video-resolution":"Video Resolution","warranty-period":"Warranty period","warranty-type":"Warranty type"},"items":[{"model":"Kile-KILEAPPLEIP7P32BK, Gold, Jet Black, Kile-KILEAPPLEIP7P256RD, Apple","storage":"32GB, 128GB, 256GB","inbox":["1 x iPhone 7 Plus","1 x Apple EarPods","1 x Lightning Cable","1 x USB Adapter","1 x Lightning to 3.5mm adapter"],"general_info":{"sku":"AP564ELAA552AISGAMZ-10085219","condition":"New","screen-size-inches":"4.7","model":"Jet Black","phone-features":"Dustproof / Waterproof","ram-memory":"6GB","storage-capacity":"256GB","warranty-period":"1 Year","warranty-type":"Local (Singapore) manufacturer warranty"},"delivery":{"info":[{"label":"Fastest delivery option available for FREE with LiveUp","time":"Get by Tue, 8 - Fri, 11 Aug 2017"},{"label":"Economy Delivery: Free","time":"Get by Thu, 10 - Mon, 14 Aug 2017"},{"label":"Standard Delivery: SGD 2.99","time":"Get by Tue, 8 - Fri, 11 Aug 2017"}]},"top_review":[{"rating":5,"title":"Five Stars","comment":"Received item as it is.","author":"Lincoln T."},{"rating":1,"title":"Did not Receive the Order","comment":"This Seller is a fraud. I paid for that mobile. I completed everything and even paid extra for Standard Delivery but I never get my item uptill Now. He is a theif. \nYesterday was the maximum last day for my order to be delivered and here it is still IN PROCESSING. Now, neither this seller is replying to the LAZADA CENTER.","author":"Shahjahan R."},{"rating":5,"title":"Delivery fast and item is still sealed","comment":"Received 2 days after placing order. Look like the new set. Manage to register warranty once I activated the phone.","author":"Mai L."},{"rating":5,"title":"Nice price","comment":"256 is cheaper than 128!!!???","author":"Hajime I."},{"rating":5,"title":"Five Stars","comment":"Received my 7 plus jet black with good conditions. Thx","author":"Lazada Guest"},{"rating":1,"title":"hjejrrr","comment":"thrkriri444","author":"Lazada Guest"}],"seller":{"name":"Boom","url":"http://www.lazada.sg/boom_","rating":2.15},"title":"Apple iPhone 7 Plus 256GB (Jet Black)","img":"https://sg-live-01.slatic.net/p/2/apple-iphone-7-plus-256gb-jet-black-1473743575-0334368-eca1d03c6458f09a50585ee283270f56-zoom.jpg","special_price":"SGD 1,264.00","before_price":"SGD 1,588.00,","off_price":"20% off","avr_rating":"3.7","total_rating_review":"6 ratings 6 reviews","warranty":"1 YearLocal (Singapore) manufacturer warranty","highlight":["5.5\" Retina HD wide colour gamut display","IP67 water and dust resistant","Dual 12 MP main camera (wide colour capture, f/1.8, optical\nimage stabilization (OIS), 2x optical zoom, Quad LED true tone\nflash)","7 MP front camera (FaceTime HD, wide colour capture)","Built-in stereo speakers","iOS 10","1 year local Apple Singapore warranty (starts on date of\npurchase)"]},{"model":"Galaxy S8, Galaxy S8+","color":"Gold, Black, Grey","inbox":["1 x Samsung S8 64GB","1 x Samsung USB Cable","1 x Samsung USB Charger","1 x AKG Headset","1 x Manual"],"general_info":{"sku":"SA356ELAAAT4XXSGAMZ-21793180","camera-back":"11 to 15MP","camera-front":"8 MP","condition":"New","screen-size-inches":"5.8","model":"Galaxy S8","network-connections":"4G","operating-system":"Android","operating-system-version":"Android","phone-features":"Dustproof / Waterproof|Expandable Memory|Fingerprint Sensor|Touchscreen|GPS|WiFi","ppi":"Above 500 PPI","processor-type":"Octa-core","ram-memory":"4GB","resolution":"QuadHD","screen-type":"AMOLED","storage-capacity":"64GB","type-of-battery":"Built-in Rechargeable Battery","video-resolution":"1080p","warranty-period":"1 Year","warranty-type":"Local (Singapore) manufacturer warranty"},"delivery":{},"top_review":[{"rating":5,"title":"Regarding phone","comment":"This phone can i use in india","author":"Sandhu"}],"seller":{"name":"Boom","url":"http://www.lazada.sg/boom_","rating":2.15},"title":"Samsung Galaxy S8 64GB (Midnight Black)","img":"https://sg-live-02.slatic.net/p/2/samsung-galaxy-s8-64gb-midnight-black-1493001194-98555181-a261d6a8e94491d6fcd766fcdbe8f646-zoom.jpg","special_price":"SGD 858.00","before_price":"SGD 1,068.00,","off_price":"20% off","avr_rating":"5","total_rating_review":"1 rating 1 review","warranty":"1 YearLocal (Singapore) manufacturer warranty","highlight":["5.8\" Quad HD (1440 x 2960 pixel) Super AMOLED display","10nm Octa-core","IP68 certified dust/water proof","4 GB RAM","64 GB internal storage (expandable up to 256GB with microSD\nsupport)","Iris scanner, fingerprint scanner, always-on display"]}]};
    // res.send(data.items);
    res.render('compare', {
        title: "Compare Products",
        data
    });
});
router.get('/compare', function(req, res, next) {
    let urlPattern = new RegExp("(http|ftp|https)://[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:/~+#-]*[\w@?^=%&amp;/~+#-])?");
    let product = [req.query.product1, req.query.product2];
    // let product = ['http://www.lazada.sg/telco-apple-iphone-7-16461012.html', 'http://www.lazada.sg/motorola-moto-e4-16gb-lte-free-motorola-gift-box-58369530.html'];
    let promises = [];
    if (urlPattern.test(product[0]) && urlPattern.test(product[1])) {
        let info_list = {};
        let general_list = {};
        for (let i = 0; i < product.length; i++) {
            let url = new Promise(function(resolve, reject) {
                request(product[i], function(error, response, body) {
                    if (error) {
                        reject(error);
                    } else {
                        const $ = cheerio.load(body);
                        let html = {
                            url: product[i]
                        };
                        // Highlight
                        let hl_arr = [];
                        $('div.prod_details li span').each(function() {
                            hl_arr.push($(this).text().trim());
                        });
                        // Variants
                        $('div.prod_grouped div.prod-multi-group').each(function() {
                            $(this).find('div.prod-multi-group__title span').remove();
                            let v_name = $(this).find('div.prod-multi-group__title').text().trim().replace(':', '');
                            if (v_name.length > 0) {
                                if (v_name == 'Choose a color') {
                                    v_name = 'Color';
                                } else if (v_name == 'Storage Capacity') {
                                    v_name = 'Storage';
                                }
                                let v_value = [];
                                $(this).find('li.prod-multi-group__item').each(function() {
                                    v_value.push($(this).find('.prod-multi-group__item-value').text().trim());
                                });
                                info_list[furl(v_name)] = v_name;
                                html[furl(v_name)] = v_value.join(', ');
                            }
                        });
                        // Product description inbox
                        let pro_desc_inbox = $('.product-description__block .product-description__inbox');
                        if (pro_desc_inbox.length > 0) {
                            let pro_desc_inbox_title = pro_desc_inbox.find('.product-subheader__element').text().trim().replace(':', '');
                            let pro_desc_inbox_content = [];
                            pro_desc_inbox.find('.ui-listBulleted li.inbox__item').each(function() {
                                pro_desc_inbox_content.push($(this).find('span').text().trim());
                            });
                            // html.info_list = info_list;
                            html.inbox = pro_desc_inbox_content;
                        }
                        // Product General Feature
                        let general = $('.product-description__block table.specification-table');
                        let general_info = {};
                        $('.product-description__block table.specification-table tr').each(function() {
                            let g_name = $(this).find('td:first-child').text().trim();
                            let g_value = $(this).find('td:last-child').text().trim();
                            general_list[furl(g_name)] = g_name;
                            general_info[furl(g_name)] = g_value;
                        });
                        // html.general_list = general_list;
                        html.general_info = general_info;
                        // Delivery
                        let delivery = $('.delivery-info');
                        html.delivery = {};
                        if (delivery.find('.fulfillment').length > 0) {
                            html.delivery.fullfilled = delivery.find('.fulfillment__message').text().trim();
                        }
                        if (delivery.find('.delivery-types__data .product__promotion-item').length > 0) {
                            let delivery_info = [];
                            delivery.find('.delivery-types__data .product__promotion-item').each(function() {
                                let delivery_obj = {
                                    label: $(this).find('.delivery-option-st__label').text().trim(),
                                    time: $(this).find('.delivery-option-st__time').text().trim()
                                };
                                delivery_info.push(delivery_obj);
                            });
                            html.delivery.info = delivery_info;
                        }
                        // Seller
                        let seller_box = $('.seller-details');
                        let seller_info = {};
                        seller_info.name = seller_box.find('.basic-info__name').text().trim();
                        if (seller_box.find('.basic-info__name').attr('href')) {
                            seller_info.url = seller_box.find('.basic-info__name').attr('href').trim();
                        }
                        if (seller_box.find('.seller-rating__stars-cont_active').length > 0) {
                            let seller_rate = seller_box.find('.seller-rating__stars-cont_active').attr('style').trim();
                            seller_rate = parseInt(seller_rate.replace(/[^0-9\.]/g, ''), 10);
                            seller_rate = seller_rate * 5 / 100;
                            seller_rate = parseFloat(seller_rate);
                            seller_rate = Math.round(seller_rate * 1000) / 1000;
                            seller_info.rating = seller_rate;
                        }
                        // Review
                        let review_box = $('.c-review-list__list');
                        let review_arr = [];
                        review_box.find('.c-review-list__item').each(function() {
                            let review_info = {};
                            let rating = $(this).find('.c-rating-stars__active').attr('style').trim();
                            rating = parseInt(rating.replace(/[^0-9\.]/g, ''), 10);
                            rating = rating * 5 / 100;
                            rating = parseFloat(rating);
                            rating = Math.round(rating * 1000) / 1000;
                            review_info.rating = rating;
                            review_info.title = $(this).find('.c-review__title').text().trim();
                            review_info.comment = $(this).find('.c-review__comment').text().trim();
                            review_info.author = $(this).find('.c-review__name-text').text().trim();
                            review_arr.push(review_info);
                        });
                        html.top_review = review_arr;
                        html.seller = seller_info;
                        html.title = $('h1#prod_title').text().trim();
                        html.img = $('div#productZoom').data('zoom-image');
                        html.special_price = $('span#special_currency_box').text().trim() + ' ' + $('span#special_price_box').text().trim();
                        if (!$('#special_price_area').hasClass('hidden')) {
                            html.before_price = $('span#price_box').text().trim();
                            html.off_price = $('span#product_saving_percentage').text().trim() + ' off';
                        }
                        html.avr_rating = $('div.c-rating-total__text-rating-average em').text().trim();
                        html.total_rating_review = $('div.c-rating-total__text-total-review').text().trim();
                        html.warranty = $('div.prod_brief .prod-warranty > span.prod-warranty__term').text().trim() + $('div.prod_brief .prod-warranty > span.prod-warranty__type').text().trim();
                        html['highlight'] = hl_arr;
                        resolve(html);
                    }
                });
            });
            promises.push(url);
        }
        promise.all(promises).then(function(result) {
            let data = {
                info_list: info_list,
                general_list: general_list,
                items: result
            };
            // res.send(data);
            res.render('compare', {
                title: 'Compare Products',
                data
            });
        }, function(err) {
            console.log(err);
        });
    } else {
        res.send("URL is invalid");
    }
});

function dataHandle(array) {}
module.exports = router;
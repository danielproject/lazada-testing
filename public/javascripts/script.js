$(function() {
    $('.review-comment').each(function() {
        let comment = $(this).find('.comment');
        if (comment.text().length > 100) {
            comment.attr('data-comment', comment.text());
            let str = comment.text();
            var maxLength = 100;
            var trimmedString = str.substr(0, maxLength);
            trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))
            comment.text(trimmedString);
            comment.addClass('deactive');
            comment.parent().append('<a href="javascript:void(0)">Read more...</a>');
        }
    });
    $('.review-comment').on('click', 'a', function() {
        let comment = $(this).closest('.review-comment').find('.comment');
        let data_comment = comment.attr('data-comment');
        let text = comment.text();
        if (comment.hasClass('deactive')) {
            comment.removeClass('deactive').addClass('active');
            $(this).text('Read less...');
        } else if (comment.hasClass('active')) {
            comment.removeClass('active').addClass('deactive');
            $(this).text('Read more...');
        }
        comment.text(data_comment).attr('data-comment', text);
    });
})